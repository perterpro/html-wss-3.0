(function($) {
    'use strict';

    $(document).ready(function () {
        if($('.navigation-wrap-banner .banner-carousel').length > 0) {
            $('.navigation-wrap-banner .banner-carousel').owlCarousel({
                autoplay: true,
                loop: true,
                nav: true,
                dots: false,
                lazyLoad: true,
                autoplayHoverPause: true,
                lazyLoadEager: 1,
                items: 1
            });
        }

        if($('.deal-product-title li a').length > 0 && $('.deal-product-content .deal-product-panel').length > 0) {
            $(document).on('click', '.deal-product-title li a', function(e) {
                e.preventDefault();
                $('.deal-product-title li a').removeClass('active');
                $(this).addClass('active');
                $('.deal-product-content .deal-product-panel').hide();
                $($(this).attr('href')).show();
            });
        }
		
		if($('.menu-main > ol').length > 0) {
            $('.menu-main > ol').niceScroll({
                horizrailenabled: false
            });
        }

		$('.menu-main li.has-child').hover(function() {
            $('.menu-main li.has-child .submenu').removeAttr('style');

		    var position = $(this).position();
		    var top = position.top;
		    var hCurrent = $(this).outerHeight();
		    var hSubmenu = $(this).find('.submenu').outerHeight();
		    var htWindow = $(window).height();

		    if(hSubmenu > htWindow - top) {
                $(this).find('.submenu').css('top', (top + hCurrent - hSubmenu) + 'px');
            } else {
                $(this).find('.submenu').css('top', top + 'px');
            }
        });
    });
})(jQuery);