// Web Font Loader
WebFontConfig = {
    google: {
        families: [ 'Roboto:400,400i,500,500i,700,700i&display=swap' ]
    }
};

(function(d) {
    var wf = d.createElement('script'), s = d.scripts[0];
    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
    wf.async = true;
    s.parentNode.insertBefore(wf, s);
})(document);

// Default
(function($) {
    'use strict';

    $(document).ready(function () {
        if($('.brand-wrap .brand-carousel').length > 0) {
            $('.brand-wrap .brand-carousel').owlCarousel({
                autoplay: true,
                loop: true,
                nav: true,
                dots: false,
                lazyLoad: true,
                autoplayHoverPause: true,
                responsive: {
                    0: {
                        lazyLoadEager: 8,
                        items: 8,
                        margin: 20
                    }
                }
            });
        }

        scrollToTop();

        $('#wrongPrice').validate({
            rules: {
                'reason': {
                    'wrongPrice': true
                },
                'comment': {
                    'wrongPrice': true
                }
            },
            submitHandler: function(form) {
                $.ajax({
                    url: form.action,
                    type: form.method,
                    data: $(form).serialize(),
                    success: function(response) {
                        $('.wrong-price-success').addClass('active');
                    },
                    error: function (errorThrown) {
                        console.log(errorThrown);
                    }
                });
            }
        });

        $.validator.addMethod('wrongPrice', function (value, element) {
            var check = false;
            var reason = $('input[name="reason"]:checked').map(function(_, el) {
                return $(el).val();
            }).get();

            if(reason.length !== 0) {
                $('.wrong-price-reason .wrong-error-text').html('').hide();
                $('.wrong-price-checkbox label').removeClass('wrong-error');

                if($.inArray('5', reason) >= 0) {
                    var comment = $('#comment').val();
                    if(comment !== '') {
                        $('.wrong-price-comment .wrong-error-text').html('').hide();
                        $('.wrong-price-comment textarea').removeClass('wrong-error');
                        check = true;
                    } else {
                        $('.wrong-price-comment textarea').addClass('wrong-error');
                        $('.wrong-price-comment .wrong-error-text').html('Nội dung bắt buộc, vui lòng điền thông tin').show();
                    }
                } else {
                    $('.wrong-price-comment .wrong-error-text').html('').hide();
                    $('.wrong-price-comment textarea').removeClass('wrong-error');
                    check = true;
                }
            } else {
                $('.wrong-price-reason .wrong-error-text').html('Nội dung bắt buộc, vui lòng chọn tối thiểu 1 vấn đề!').show();
                $('.wrong-price-checkbox label').addClass('wrong-error');
            }

            return check;
        }, '');
    });

    $(document).on('click', '.scroll-top', function() {
        $('html, body').animate({
                scrollTop: 0
            }, 'slow');

        return false;
    });

    $(document).on('click', '.login-wrap-text', function() {
        $('.login-popup').toggle();
    });

    $(document).on('click', function(e) {
        var container = $('.login-wrap');

        if(!container.is(e.target) && container.has(e.target).length === 0 && container.is(':visible')) {
            $('.login-popup').hide();
        }
    });

    $(window).scroll(function() {
        scrollToTop();
        closePopupLogin();
    });

    function closePopupLogin() {
        var offset = $('.top-bar').outerHeight();
        var scroll = $(window).scrollTop();

        if(scroll > offset && $('.login-popup').is(':visible')) {
            $('.login-popup').hide();
        }
    }

    function scrollToTop() {
        var offset = $(window).height();
        var scroll = $(window).scrollTop();

        if(scroll > offset) {
            $('.scroll-top').fadeIn();
        } else {
            $('.scroll-top').fadeOut();
        }
    }

    $(document).on('click', '.wrong-price .wrong-price-title', function(e) {
        e.preventDefault();
        $('.wrong-price').toggleClass('active');
    });

    $(document).on('click', '.wrong-success-back span', function(e) {
        e.preventDefault();
        $('.wrong-price-success').removeClass('active');
    });
})(jQuery);