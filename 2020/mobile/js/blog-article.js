(function($) {
    'use strict';

    $(document).ready(function () {
        if($('.deal-product-nav li a').length > 0 && $('.deal-product-content .deal-product-panel').length > 0) {
            $(document).on('touchstart', '.deal-product-nav li a', function(e) {
                $('.deal-product-nav li a').removeClass('active');
                $(this).addClass('active');
                $('.deal-product-content .deal-product-panel').hide();
                $($(this).attr('href')).fadeIn();
            });
        }

        $(document).on('touchstart', '.toc-wrap span', function() {
            $(this).toggleText('[xem]', '[ẩn]');
            $('.toc-wrap ul').slideToggle();
        });

        menuMobileSticky();
    });

    $(window).scroll(function() {
        menuMobileSticky();
    });

    function menuMobileSticky() {
        var offset = $('.header-wrap').outerHeight();
        var scroll = $(window).scrollTop();

        if(scroll > offset) {
            $('.search-header').addClass('fixed');
        } else {
            $('.search-header').removeClass('fixed');
        }
    }

    $.fn.extend({
        toggleText: function(a, b){
            return this.text(this.text() == b ? a : b);
        }
    });
})(jQuery);