// Web Font Loader
WebFontConfig = {
    google: {
        families: [ 'Roboto:400,400i,500,500i,700,700i&display=swap' ]
    }
};

(function(d) {
    var wf = d.createElement('script'), s = d.scripts[0];
    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
    wf.async = true;
    s.parentNode.insertBefore(wf, s);
})(document);

(function($) {
    'use strict';

    $(document).ready(function () {
        $(document).on('click', '.menu-mobile-btn', function() {
            $('.menu-mobile-wrap').toggleClass('active');
            $('body').toggleClass('no-scroll');
            $('.search-header').removeClass('fixed');
        });

        $(document).on('touchstart', function(e) {
            var container = $('.menu-mobile-wrap, .filter-wrap');

            if(!container.is(e.target) && container.has(e.target).length === 0 && container.is(':visible')) {
                container.removeClass('active');
                $('body').removeClass('no-scroll');
            }
        });

        $(document).on('touchstart', '.menu-mobile-wrap .menu-label-child, .menu-mobile-wrap .submenu .menu-has-child', function(e) {
            var elm = $(this).next();

            if($(this).parent().hasClass('menu-mobile-label')) {
                $('.menu-mobile-wrap .menu-label-child .menu-has-child').removeClass('active');

                if(!elm.is(":visible")) {
                    $(this).find('.menu-has-child').addClass('active');
                    $('.menu-mobile-wrap .menu-mobile-label > .submenu').slideUp();
                }
            } else {
                $('.menu-mobile-wrap .submenu .menu-has-child').removeClass('active');

                if(!elm.is(":visible")) {
                    $(this).addClass('active');
                    $('.menu-mobile-wrap .submenu .submenu').slideUp();
                }
            }

            elm.slideToggle();
        });

        $(document).on('touchstart', '.search-header-wrap .search-form, .search-wrap .search-wrap-close', function(e) {
            $('.search-wrap').toggleClass('active');
            $('body').toggleClass('no-scroll-background');
            $('.search-header').removeClass('fixed');

            setTimeout(function () {
                if($('.search-wrap').hasClass('active')) {
                    $('.search-wrap .search-input').focus();
                } else {
                    $('.search-wrap .search-input').blur();
                }
            }, 400);
        });

        $('#wrongPrice').validate({
            rules: {
                'reason': {
                    'wrongPrice': true
                },
                'comment': {
                    'wrongPrice': true
                }
            },
            submitHandler: function(form) {
                $.ajax({
                    url: form.action,
                    type: form.method,
                    data: $(form).serialize(),
                    success: function(response) {
                        $('.wrong-price-wrap').addClass('success');
                        $('.wrong-price-success').addClass('active');
                    },
                    error: function (errorThrown) {
                        console.log(errorThrown);
                    }
                });
            }
        });

        $.validator.addMethod('wrongPrice', function (value, element) {
            var check = false;
            var reason = $('input[name="reason"]:checked').map(function(_, el) {
                return $(el).val();
            }).get();

            if(reason.length !== 0) {
                $('.wrong-price-reason .wrong-error-text').html('').hide();
                $('.wrong-price-checkbox label').removeClass('wrong-error');

                if($.inArray('5', reason) >= 0) {
                    var comment = $('#comment').val();
                    if(comment !== '') {
                        $('.wrong-price-comment .wrong-error-text').html('').hide();
                        $('.wrong-price-comment textarea').removeClass('wrong-error');
                        check = true;
                    } else {
                        $('.wrong-price-comment textarea').addClass('wrong-error');
                        $('.wrong-price-comment .wrong-error-text').html('Nội dung bắt buộc, vui lòng điền thông tin').show();
                    }
                } else {
                    $('.wrong-price-comment .wrong-error-text').html('').hide();
                    $('.wrong-price-comment textarea').removeClass('wrong-error');
                    check = true;
                }
            } else {
                $('.wrong-price-reason .wrong-error-text').html('Nội dung bắt buộc, vui lòng chọn tối thiểu 1 vấn đề!').show();
                $('.wrong-price-checkbox label').addClass('wrong-error');
            }

            return check;
        }, '');
    });

    $(document).on('touchstart', '.wrong-price-icon, .wrong-price .search-wrap-close', function(e) {
        e.preventDefault();
        $('.wrong-price').toggleClass('active');
        $('body').toggleClass('no-scroll-background');
        $('.search-header').removeClass('fixed');
    });

    $(document).on('click', '.wrong-success-back span', function(e) {
        e.preventDefault();
        $('.wrong-price-wrap').removeClass('success');
        $('.wrong-price-success').removeClass('active');
    });
})(jQuery);