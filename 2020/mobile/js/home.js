(function($) {
    'use strict';

    $(document).ready(function () {
        if($('.banner-carousel.owl-carousel').length > 0) {
            $('.banner-carousel.owl-carousel').owlCarousel({
                autoplay: true,
                loop: true,
                nav: true,
                dots: true,
                lazyLoad: true,
                autoplayHoverPause: true,
                lazyLoadEager: 1,
                items: 1
            });
        }

        if($('.deal-product-nav h3').length > 0 && $('.deal-product-content .deal-product-panel').length > 0) {
            $(document).on('touchstart', '.deal-product-nav h3', function(e) {
                $('.deal-product-nav h3').removeClass('active');
                $(this).addClass('active');
                $('.deal-product-content .deal-product-panel').hide();
                $($(this).data('panel')).fadeIn();
            });
        }

        menuMobileSticky();
    });

    $(window).scroll(function() {
        menuMobileSticky();
    });

    function menuMobileSticky() {
        var offset = $('.header-wrap').outerHeight();
        var scroll = $(window).scrollTop();

        if(scroll > offset) {
            $('.search-header').addClass('fixed');
        } else {
            $('.search-header').removeClass('fixed');
        }
    }
})(jQuery);