(function($) {
    'use strict';

    $(document).ready(function () {
        if($('.deal-product-nav li a').length > 0 && $('.deal-product-content .deal-product-panel').length > 0) {
            $(document).on('touchstart', '.deal-product-nav li a', function() {
                $('.deal-product-nav li a').removeClass('active');
                $(this).addClass('active');
                $('.deal-product-content .deal-product-panel').hide();
                $($(this).attr('href')).fadeIn();
            });
        }

        $(document).on('touchstart', '.submenu-blog-more span', function() {
            $(this).toggleText('Mở rộng', 'Thu lại').toggleClass('active').parent().prev().toggleClass('active');
        });

        menuMobileSticky();
    });

    $(window).scroll(function() {
        menuMobileSticky();
    });

    function menuMobileSticky() {
        var offset = $('.header-wrap').outerHeight();
        var scroll = $(window).scrollTop();

        if(scroll > offset) {
            $('.search-header').addClass('fixed');
        } else {
            $('.search-header').removeClass('fixed');
        }
    }

    $.fn.extend({
        toggleText: function(a, b){
            return this.text(this.text() == b ? a : b);
        }
    });
})(jQuery);