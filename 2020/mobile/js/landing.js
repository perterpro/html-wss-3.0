// Web Font Loader
WebFontConfig = {
    google: {
        families: [ 'Roboto:400,400i,500,500i,700,700i&display=swap' ]
    }
};

(function(d) {
    var wf = d.createElement('script'), s = d.scripts[0];
    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
    wf.async = true;
    s.parentNode.insertBefore(wf, s);
})(document);

(function($) {
    'use strict';

    $(document).ready(function () {
        $(document).on('click', '.menu-mobile-btn', function() {
            $('.menu-mobile-wrap').toggleClass('active');
            $('body').toggleClass('no-scroll');
            $('.search-header').removeClass('fixed');
        });

        $(document).on('touchstart', function(e) {
            var container = $('.menu-mobile-wrap, .filter-wrap');

            if(!container.is(e.target) && container.has(e.target).length === 0 && container.is(':visible')) {
                container.removeClass('active');
                $('body').removeClass('no-scroll');
            }
        });

        $(document).on('touchstart', '.menu-mobile-wrap .menu-label-child, .menu-mobile-wrap .submenu .menu-has-child', function(e) {
            var elm = $(this).next();

            if($(this).parent().hasClass('menu-mobile-label')) {
                $('.menu-mobile-wrap .menu-label-child .menu-has-child').removeClass('active');

                if(!elm.is(":visible")) {
                    $(this).find('.menu-has-child').addClass('active');
                    $('.menu-mobile-wrap .menu-mobile-label > .submenu').slideUp();
                }
            } else {
                $('.menu-mobile-wrap .submenu .menu-has-child').removeClass('active');

                if(!elm.is(":visible")) {
                    $(this).addClass('active');
                    $('.menu-mobile-wrap .submenu .submenu').slideUp();
                }
            }

            elm.slideToggle();
        });

        $(document).on('touchstart', '.search-header-wrap .search-form, .search-wrap .search-wrap-close', function(e) {
            $('.search-wrap').toggleClass('active');
            $('body').toggleClass('no-scroll-background');
            $('.search-header').removeClass('fixed');

            if($('.search-wrap').hasClass('active')) {
                $('.search-wrap .search-input').focus();
            } else {
                $('.search-wrap .search-input').blur();
            }
        });

        if($('.feature-owl').length > 0) {
            $('.feature-owl').owlCarousel({
                autoplay: true,
                loop: true,
                nav: false,
                dots: true,
                lazyLoad: true,
                autoplayHoverPause: true,
                lazyLoadEager: 1,
                items: 1
            });
        }

        if($('.customer-owl').length > 0) {
            $('.customer-owl').owlCarousel({
                autoplay: true,
                loop: true,
                nav: true,
                dots: false,
                lazyLoad: true,
                autoplayHoverPause: true,
                lazyLoadEager: 1,
                items: 1,
                navText: ['<i class="slider-button slider-left"></i>', '<i class="slider-button slider-right"></i>']
            });
        }

        $('#registerSuccess').fancybox();
        $('#registerForm').validate({
            rules: {
                email: {
                    'required' : true,
                    'email' : true,
                    'validateEmail': true
                },
                phone: {
                    'required' : true,
                    'number' : true,
                    'rangelength': [10, 10]
                },
                store: 'required',
                website: 'required',
                address: 'required'
            },
            messages: {
                email: {
                    required: 'Nội dung bắt buộc, vui lòng điền thông tin!',
                    email: 'Sai định dạng, vui lòng điền đúng email!',
                    validateEmail: 'Sai định dạng, vui lòng điền đúng email!'
                },
                phone: {
                    required: 'Nội dung bắt buộc, vui lòng điền thông tin!',
                    number: 'Sai định dạng, vui lòng điền đúng số điện thoại!',
                    rangelength: 'Sai định dạng, vui lòng điền đúng số điện thoại!'
                },
                store: {
                    required: 'Nội dung bắt buộc, vui lòng điền thông tin!'
                },
                website: {
                    required: 'Nội dung bắt buộc, vui lòng điền thông tin!'
                },
                address: {
                    required: 'Nội dung bắt buộc, vui lòng điền thông tin!'
                }
            },
            submitHandler: function(form) {
                $.ajax({
                    url: form.action,
                    type: form.method,
                    data: $(form).serialize(),
                    success: function(response) {
                        $('#registerSuccess').trigger('click');
                    },
                    error: function (errorThrown) {
                        console.log(errorThrown);
                    }
                });
            }
        });

        $.validator.addMethod('validateEmail', function (value, element) {
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            return emailReg.test(value);
        }, '');
    });

    $(window).scroll(function() {
        menuMobileSticky();
    });

    function menuMobileSticky() {
        var offset = $('.header-wrap').outerHeight();
        var scroll = $(window).scrollTop();

        if(scroll > offset) {
            $('.search-header').addClass('fixed');
        } else {
            $('.search-header').removeClass('fixed');
        }
    }

    $(document).on('click', '.button-register', function(e) {
        e.preventDefault();

        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top - 60
        }, 'slow');
    });
})(jQuery);