(function($) {
    'use strict';

    $(document).ready(function() {
        $(document).on('click', '.search-wrap .search-expand', function () {
            $(this).toggleText('Mở rộng điều kiện tìm kiếm [ + ]', 'Thu gọn điều kiện tìm kiếm [ - ]');
            $('.search-wrap .search-row').toggleClass('expand-active')
        });
    });

    $.fn.extend({
        toggleText: function(a, b){
            return this.text(this.text() == b ? a : b);
        }
    });
})(jQuery);