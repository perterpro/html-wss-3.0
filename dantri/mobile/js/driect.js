// Web Font Loader
WebFontConfig = {
    google: {
        families: [ 'Roboto:400,400i,500,500i,700,700i&display=swap' ]
    }
};

(function(d) {
    var wf = d.createElement('script'), s = d.scripts[0];
    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
    wf.async = true;
    s.parentNode.insertBefore(wf, s);
})(document);

// Default
(function($) {
    'use strict';

    $(document).ready(function () {
        var myTimer = document.getElementById('driectTimer');
        var t = 5;
        var intervalCounter = 0;

        window.timer = window.setInterval(function () {
            intervalCounter ++;
            myTimer.innerHTML = (t - intervalCounter) + 's';

            if (intervalCounter >= 5) {
                window.clearInterval(window.timer);
            }
        }.bind(this), 1000);
    });
})(jQuery);