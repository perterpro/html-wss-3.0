(function($) {
    'use strict';

    $(document).ready(function () {
        $(document).on('touchstart', '.read-more span', function() {
            $(this).toggleText('Xem thêm', 'Rút gọn');
            $('.category-description-wrap').toggleClass('active');
        });

        if($('.price-range').length > 0 && $('.price-amount').length > 0) {
            var rangeSlider = document.getElementById('price-range');
            noUiSlider.create(rangeSlider, {
                connect: true,
                start: [0, 200],
                step: 1,
                range: {
                    'min': 0,
                    'max': 200
                }
            });

            rangeSlider.noUiSlider.on('update', function (values) {
                $('.price-amount').html( '<span>' + Number(values[0]) + '</span><span>' + Number(values[1]) + ' triệu</span>' );
            });
        }

        $(document).on('click', '.filter-close, .filter-icon', function () {
            $('.filter-wrap').toggleClass('active');
            $('body').toggleClass('no-scroll');
            $('.search-header').toggleClass('relative').removeClass('fixed');
        });

        $(document).on('touchstart', '.toc-wrap span', function() {
            $(this).toggleText('[xem]', '[ẩn]');
            $('.toc-wrap ul').slideToggle();
        });

        $(document).on('touchstart', '.toc-wrap a', function(e) {
            $('html, body').animate({
                scrollTop: $($(this).attr('href')).offset().top
            }, 'slow');
        });

        menuMobileSticky();
    });

    $(window).scroll(function() {
        menuMobileSticky();
    });

    function menuMobileSticky() {
        $('.search-header').removeClass('relative');
        var offset = $('.header-wrap').outerHeight();
        var scroll = $(window).scrollTop();

        if(scroll > offset) {
            $('.search-header').addClass('fixed');
        } else {
            $('.search-header').removeClass('fixed');
        }
    }

    $.fn.extend({
        toggleText: function(a, b){
            return this.text(this.text() == b ? a : b);
        }
    });
})(jQuery);