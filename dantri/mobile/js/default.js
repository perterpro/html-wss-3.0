// Web Font Loader
WebFontConfig = {
    google: {
        families: [ 'Roboto:400,400i,500,500i,700,700i&display=swap' ]
    }
};

(function(d) {
    var wf = d.createElement('script'), s = d.scripts[0];
    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
    wf.async = true;
    s.parentNode.insertBefore(wf, s);
})(document);

(function($) {
    'use strict';

    $(document).ready(function () {
        $(document).on('click', '.menu-mobile-btn', function() {
            $('.menu-mobile-wrap').toggleClass('active');
            $('body').toggleClass('no-scroll');
            $('.search-header').removeClass('fixed');
        });

        $(document).on('touchstart', function(e) {
            var container = $('.menu-mobile-wrap, .filter-wrap');

            if(!container.is(e.target) && container.has(e.target).length === 0 && container.is(':visible')) {
                container.removeClass('active');
                $('body').removeClass('no-scroll');
            }
        });

        $(document).on('touchstart', '.search-header-wrap .search-form, .search-wrap .search-wrap-close', function(e) {
            $('.search-wrap').toggleClass('active');
            $('body').toggleClass('no-scroll-background');
            $('.search-header').removeClass('fixed');

            if($('.search-wrap').hasClass('active')) {
                $('.search-wrap .search-input').focus();
            } else {
                $('.search-wrap .search-input').blur();
            }
        });
    });
})(jQuery);