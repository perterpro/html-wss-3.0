(function($) {
    'use strict';

    $(document).ready(function () {
        if($('.banner-carousel').length > 0) {
            $('.banner-carousel').owlCarousel({
                autoplay: true,
                loop: true,
                nav: true,
                dots: false,
                lazyLoad: true,
                autoplayHoverPause: true,
                lazyLoadEager: 1,
                items: 1
            });
        }

        if($('.banner-owl').length > 0) {
            $('.banner-owl').owlCarousel({
                autoplay: true,
                loop: false,
                nav: true,
                dots: false,
                lazyLoad: true,
                autoplayHoverPause: true,
                lazyLoadEager: 3,
                items: 3,
                margin: 28
            });
        }

		if($('.menu-main ol').length > 0) {
            $('.menu-main ol').niceScroll({
                horizrailenabled: false
            });
        }
    });
})(jQuery);